package net.eneiluj.moneybuster.service;

import android.app.Notification;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import net.eneiluj.moneybuster.R;
import net.eneiluj.moneybuster.model.DBBill;
import net.eneiluj.moneybuster.model.DBBillOwer;
import net.eneiluj.moneybuster.model.DBNotificationTracker;
import net.eneiluj.moneybuster.model.DBPaymentMode;
import net.eneiluj.moneybuster.model.DBProject;
import net.eneiluj.moneybuster.model.ProjectType;
import net.eneiluj.moneybuster.persistence.MoneyBusterSQLiteOpenHelper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NotificationTrackerService extends NotificationListenerService {
    private static class StringNotFoundException extends Exception {

        public StringNotFoundException(String exception) {
        }
    }

    private List<DBNotificationTracker> trackersList;

    private static final String TAG = "NotificationTrackerService";
    private static volatile boolean isRunning = false;
    private boolean hasNotificationPermission = false;
    private MoneyBusterSQLiteOpenHelper db;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        init();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    public void init() {
        isRunning = true;
        hasNotificationPermission = this.isNotificationServiceRunning();
        db = MoneyBusterSQLiteOpenHelper.getInstance(getApplicationContext());
        loadTrackersFromDB();
        if (hasNotificationPermission) {
            setupLogFiles();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        isRunning = false;
    }

    /**
     * Check if logger service is running.
     *
     * @return True if running, false otherwise
     */
    public static boolean isRunning() {
        return isRunning;
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        processNotification(sbn);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    private void loadTrackersFromDB() {
        trackersList = db.getNotificationTrackers();
    }

    private void setupLogFiles() {
//        create file for each package
        trackersList.forEach(new Consumer<DBNotificationTracker>() {
            @Override
            public void accept(DBNotificationTracker notificationTracker) {
                File path = getFilesDir();
                File file = new File(path, "moneytracker-" + notificationTracker.getPackageId() + ".txt");
                if (!file.exists()) {
                    FileOutputStream stream = null;
                    try {
                        stream = new FileOutputStream(file);
                    } catch (FileNotFoundException e) {
                        Log.e(TAG, "File creation error " + notificationTracker.getPackageId() + " with path " + file.getAbsolutePath());
                        e.printStackTrace();
                    }
                    try {
                        try {
                            assert stream != null;
                            stream.write(("NOTIFICATION TRACKING FOR PACKAGE: " + notificationTracker.getPackageId()).getBytes());
                            stream.close();
                            Log.d(TAG, "created file for notification tracking of package: " + notificationTracker.getPackageId() + " with path " + file.getAbsolutePath());
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.e(TAG, "File creation error " + notificationTracker.getPackageId() + " with path " + file.getAbsolutePath());

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });


    }

    private void processNotification(StatusBarNotification sbn) {
        String packageName = sbn.getPackageName();
        if (trackersList != null && trackersList.size() > 0) {
            trackersList.forEach(new Consumer<DBNotificationTracker>() {
                @Override
                public void accept(DBNotificationTracker notificationTracker) {
                    if (notificationTracker.getPackageId().equals(packageName)) {
                        Log.d(TAG, "Tracking notification from " + packageName);
                        if (notificationTracker.isIfLogToFile()) {
                            try {
                                writeLogToFile(packageName, sbn);
                            } catch (FileNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                        double value = 0;
                        try {
                            value = matchValue(sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TEXT).toString(), notificationTracker.getValueMatcher());
                        } catch (StringNotFoundException e) {
                            Log.d(TAG, "Notification dropped - no value matched");
                            return;
                        }
                        String matchedTitle = sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE).toString();
                        if (notificationTracker.getTitleMatcher().length() > 0) {
                            matchedTitle = matchTitle(matchedTitle, notificationTracker.getTitleMatcher());
                        }
                        String comment = createNotificationComment(notificationTracker);
                        if (!notificationTracker.isIfAdd()) {
                            value = -value;
                        }
                        if (value != 0) {
                            saveBill(notificationTracker.getProjectId(), matchedTitle, notificationTracker.getTrackedUserId(), value, comment, notificationTracker.getBillableUserId());
                        }

                        requestSync();

                    }
                }
            });
        }
    }

    private String createNotificationComment(DBNotificationTracker notificationTracker) {
        return new StringBuilder()
                .append("From app: " + notificationTracker.getPackageId())
                .toString();
    }

    private double matchValue(String matchString, String valueMatcher) throws StringNotFoundException {
        double d = 0;
        Pattern p = Pattern.compile("\\b\\d+\\.\\d+\\b");
        Matcher m = p.matcher(matchString);
        List<String> results = new ArrayList<>();
        while (m.find()) {
            String res = m.group();
            if (res != null) {
                try {
                    d = Double.parseDouble(m.group());
                } catch (NumberFormatException e) {
                    Log.d(TAG, "Dropped string " + res);
                }
            }
            if (d == 0) {
                throw new StringNotFoundException("Notification dropped");
            }
        }

        return d;
    }

    private String matchTitle(String matchedTitle, String titleMatcher) {
        Pattern p = Pattern.compile(titleMatcher);
        Matcher m = p.matcher(matchedTitle);
        List<String> results = new ArrayList<>();
        while (m.find()) {
            results.add(m.group());
        }
        if (results.size() > 0) {
            return results.get(0);
        } else {
            return null;
        }
    }

    private boolean usesOldPaymentModes(DBBill bill) {
        List<DBPaymentMode> userPaymentModes = db.getPaymentModes(bill.getProjectId());
        return (
                userPaymentModes.size() == 0
        );
    }

//    private String getOldPaymentModeId() {
//        int i = editPaymentMode.getSelectedItemPosition();
//        if (i < 0) {
//            return DBBill.PAYMODE_NONE;
//        } else {
//            Map<String, String> item = (Map<String, String>) editPaymentMode.getSelectedItem();
//            return item.get("id");
//        }
//    }

//    private String getRepeat() {
//        int i = editRepeat.getSelectedItemPosition();
//        if (i < 0) {
//            return DBBill.NON_REPEATED;
//        } else {
//            Map<String, String> item = (Map<String, String>) editRepeat.getSelectedItem();
//            return item.get("id");
//        }
//    }

    protected long saveBill(long projectId, String what, long payerId, double value, String comment, long billableUserId) {
        Log.d(getClass().getSimpleName(), "CUSTOM saveData()");
        String newWhat = what;
        String newComment = comment;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long newTimestamp = System.currentTimeMillis() / 1000;
        double newAmount = value;
        long newPayerId = payerId;
        String newRepeat = DBBill.NON_REPEATED;
        String newPaymentMode = DBBill.PAYMODE_CARD;
        int newPaymentModeId = DBBill.PAYMODE_ID_NONE;
        int newCategoryId = DBBill.CATEGORY_NONE;


        //   newCategoryId = getCategoryId();
        //   if (ProjectType.COSPEND.equals(projectType)) {
        //   }
        List<Long> ownersIds = new ArrayList<>();
        ownersIds.add(newPayerId);

        // if this is an existing bill

        // this is a new bill
        // add the bill
        DBBill newBill = new DBBill(
                0, 0, projectId, billableUserId, newAmount,
                newTimestamp, newWhat, DBBill.STATE_ADDED, newRepeat,
                newPaymentMode, newCategoryId, newComment, newPaymentModeId
        );
        for (long newOwnerId : ownersIds) {
            newBill.getBillOwers().add(new DBBillOwer(0, 0, newOwnerId));
        }
        long newBillId = db.addBill(newBill);

        // update last payer id
        db.updateProject(
                projectId, null, null,
                null, newPayerId, null,
                null, null,
                null, null
        );

        // normally sync should be done when we get back to bill list
        // so next line can be commented
        // db.getMoneyBusterServerSyncHelper().scheduleSync(true, bill.getProjectId());
        return newBillId;
    }

    public void requestSync() {
        Log.v(TAG, "request sync of all projects");
        List<DBProject> projs = db.getProjects();
        for (DBProject proj : projs) {
            Log.v(TAG, "request sync of project " + proj.getRemoteId());
            if (!proj.isLocal()) {
                db.getMoneyBusterServerSyncHelper().scheduleSync(false, proj.getId());
            }
        }
    }

    private void writeLogToFile(String packageId, StatusBarNotification sbn) throws FileNotFoundException {
        File path = getFilesDir();
        String fileName = "moneytracker-" + packageId + ".txt";
        File file = new File(path, fileName);

        FileOutputStream stream = null;
        stream = new FileOutputStream(file, true);
        String newLine = System.getProperty("line.separator");
        String content = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            content = new StringBuilder()
                    .append(newLine)
                    .append("New notification logged for" + packageId)
                    .append(newLine)
                    .append("============================================")
                    .append(newLine)
                    .append("Datetime: " + android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", new Date()))
                    .append(newLine)
                    .append("Title")
                    .append(newLine)
                    .append(sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE).toString())
                    .append(newLine)
                    .append("Text:")
                    .append(newLine)
                    .append(sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TEXT).toString())
                    .append(newLine)
                    .append("Category: ")
                    .append(newLine)
                    .append(sbn.getNotification().category)
                    .append(newLine)
                    .append("Channel: ")
                    .append(newLine)
                    .append(sbn.getNotification().getChannelId())
                    .append(newLine)
                    .append("Group: ")
                    .append(newLine)
                    .append(sbn.getNotification().getGroup())
                    .toString();
        } else {
            content = new StringBuilder()
                    .append(newLine)
                    .append(Arrays.toString(("New notification logged for" + packageId).getBytes()))
                    .append("============================================")
                    .append("Datetime: " + android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", new Date()))
                    .append(newLine)
                    .append("Title")
                    .append(newLine)
                    .append(sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TITLE).toString())
                    .append("Text:")
                    .append(newLine)
                    .append(sbn.getNotification().extras.getCharSequence(Notification.EXTRA_TEXT).toString())
                    .append(newLine)
                    .append("Category: ")
                    .append(newLine)
                    .append(sbn.getNotification().category)
                    .append("Group: ")
                    .append(newLine)
                    .append(sbn.getNotification().getGroup())
                    .toString();
        }
        try {
            stream.write(content.getBytes(StandardCharsets.UTF_8));
            stream.close();
        } catch (IOException e) {
            Log.e(TAG, "Cannot write log to file");
            e.printStackTrace();
        }

    }

    public boolean isNotificationServiceRunning() {
        ContentResolver contentResolver = getContentResolver();
        String enabledNotificationListeners =
                Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        String packageName = getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }
}