package net.eneiluj.moneybuster.android.activity;

import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.preference.PreferenceManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.eneiluj.moneybuster.R;
import net.eneiluj.moneybuster.android.ui.PackagesAdapter;
import net.eneiluj.moneybuster.android.ui.UserAdapter;
import net.eneiluj.moneybuster.android.ui.UserItem;
import net.eneiluj.moneybuster.model.DBMember;
import net.eneiluj.moneybuster.model.DBNotificationTracker;
import net.eneiluj.moneybuster.model.DBProject;
import net.eneiluj.moneybuster.persistence.MoneyBusterSQLiteOpenHelper;
import net.eneiluj.moneybuster.service.NotificationTrackerService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class NotificationTrackerActivity extends AppCompatActivity {
    private static final String TAG = SettingsActivity.class.getSimpleName();
    boolean hasNotificationPermissions = false;
    private LinearLayout trackingPermissionDisabledContainer;
    private LinearLayout trackingPermissionEnabledContainer;
    DBProject dbProject;
    SwitchCompat enableTrackingPermission;
    private MoneyBusterSQLiteOpenHelper db;
    private Spinner editBillableUser;
    private FloatingActionButton saveTrackerButton;
    private DBNotificationTracker dbTracker;
    private PackageManager pm;
    private CheckBox logToFileCheckbox;
    private EditText titleMatcherEditText;
    private EditText valueMatcherEditText;

    @Override
    protected void onPostResume() {
        super.onPostResume();
        loadPermissionsStatus();
        updateView();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pm = getApplicationContext().getPackageManager();
        View view = LayoutInflater.from(this).inflate(R.layout.activity_notification_tracker, null);
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        long selectedProjectId = preferences.getLong("selected_project", 0);
//        load db project
        db = MoneyBusterSQLiteOpenHelper.getInstance(NotificationTrackerActivity.this);
        dbProject = db.getProject(selectedProjectId);
        String selectedProjectName = dbProject.getName();
//        load existing tracker if exists
        setupDBNotificationTracker();
        setContentView(view);
        enableTrackingPermission = findViewById(R.id.enable_tracking_permission);
        trackingPermissionDisabledContainer = findViewById(R.id.tracking_permission_disabled_container);
        trackingPermissionEnabledContainer = findViewById(R.id.tracking_permission_enabled_container);
        TextView projectNameLabel = findViewById(R.id.project_name_label);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        editBillableUser = view.findViewById(R.id.edit_billable_user);
        setupBillableUser(editBillableUser);

        EditText searchAppEditText = view.findViewById(R.id.search_app_edit_text);
        ListView searchAppListView = view.findViewById(R.id.search_app_list_view);
        setupSelectAppView(searchAppEditText, searchAppListView);
        Spinner billingDirectionSpinner = view.findViewById(R.id.edit_billing_direction);
        setupBillingDirectionSpinner(billingDirectionSpinner);
        projectNameLabel.setText(projectNameLabel.getText().toString().replace("{projectName}", selectedProjectName));
        setSupportActionBar(toolbar);
        logToFileCheckbox = view.findViewById(R.id.log_to_file);
        setupLogfileCheckbox(logToFileCheckbox);
        titleMatcherEditText = view.findViewById(R.id.nameMatcher);
        valueMatcherEditText = view.findViewById(R.id.valueMatcher);
        setupMatchers();
        saveTrackerButton = view.findViewById(R.id.fab_save);
        saveTrackerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveOrUpdateTracker();
            }
        });
//        ask service if has permission
        enableTrackingPermission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToNotificationPermissionSettings();
            }
        });
    }

    private void setupSelectAppView(EditText searchAppEditText, ListView searchAppListView) {
        List<PackageInfo> selectedPackages = new ArrayList<>();
        List<PackageInfo> packages = pm.getInstalledPackages(0);
        final String[] currentPackage = {null};
        if (dbTracker.getPackageId() != null) {
            packages.forEach(new Consumer<PackageInfo>() {
                @Override
                public void accept(PackageInfo packageInfo) {
                    if (dbTracker.getPackageId().equals(packageInfo.applicationInfo.packageName)) {
                        currentPackage[0] = packageInfo.packageName;
                    }
                }
            });
        }

        if (currentPackage[0] != null) {
            searchAppEditText.setText(currentPackage[0]);
        }
        final boolean[] editTextLock = {false};
        searchAppListView.setVisibility(View.GONE);
        searchAppEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 2 && !editTextLock[0]) {
                    selectedPackages.clear();
                    packages.forEach(new Consumer<PackageInfo>() {
                        @Override
                        public void accept(PackageInfo packageInfo) {
                            if (packageInfo.packageName.contains(s)) {
                              selectedPackages.add(packageInfo);
                            }
                        }
                    });
                    PackagesAdapter adapter = new PackagesAdapter(NotificationTrackerActivity.this, selectedPackages, 0);
                    searchAppListView.setAdapter(adapter);
                    searchAppListView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        searchAppListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                editTextLock[0] = true;
                searchAppListView.setVisibility(View.GONE);
                searchAppEditText.setText(selectedPackages.get(position).packageName);
                editTextLock[0] = false;
                dbTracker.setPackageId(selectedPackages.get(position).packageName);
            }
        });
    }

    private void setupSelectAppSpinner(Spinner selectAppSpinner) {
        List<PackageInfo> packages = pm.getInstalledPackages(0);
        PackagesAdapter adapter = new PackagesAdapter(this, packages, 0);
        selectAppSpinner.setAdapter(adapter);
        String selectedPackage = dbTracker.getPackageId();
        if (selectedPackage != null) {
            final int[] i = {0};
            packages.forEach(new Consumer<PackageInfo>() {
                @Override
                public void accept(PackageInfo packageInfo) {
                    if (packageInfo.packageName.equals(selectedPackage)) {
                        selectAppSpinner.setSelection(i[0]);
                    }
                    i[0]++;
                }
            });
        }

        selectAppSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dbTracker.setPackageId(packages.get(position).packageName);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupMatchers() {
        titleMatcherEditText.setText(dbTracker.getTitleMatcher());
        titleMatcherEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dbTracker.setTitleMatcher(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        valueMatcherEditText.setText(valueMatcherEditText.getText().length() > 0 ? valueMatcherEditText.getText() : "\\b\\d+\\.\\d+\\b");
        valueMatcherEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dbTracker.setValueMatcher(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        dbTracker.setValueMatcher("\\b\\d+\\.\\d+\\b");
    }

    /**
     * gets current member id
     * @return
     */
    private long getCurrentUserMemberId() {
        final long[] memberId = {-1};
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String fullUserName = preferences.getString("settingsSSOUsername", null);
        if (fullUserName != null) {
            String[] splitted = fullUserName.split("@");
            List<DBMember> memberList = db.getMembersOfProject(dbProject.getId(), null);
            String userName = splitted[0];
            memberList.forEach(new Consumer<DBMember>() {
                @Override
                public void accept(DBMember dbMember) {
                    if (dbMember.getNcUserId() != null) {
                        if (dbMember.getNcUserId().equals(userName)) {
                            memberId[0] = dbMember.getId();
                        }
                    }
                }
            });
        }

        return memberId[0];
    }

    private void setupLogfileCheckbox(CheckBox logToFileCheckbox) {
        logToFileCheckbox.setChecked(dbTracker.isIfLogToFile());
        logToFileCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                dbTracker.setIfLogToFile(isChecked);
            }
        });
    }

    private void setupBillingDirectionSpinner(Spinner billingDirectionSpinner) {
        String[] arraySpinner = new String[]{
                "SUBTRACT", "ADD",
        };
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        billingDirectionSpinner.setAdapter(adapter);
        billingDirectionSpinner.setSelection(dbTracker.isIfAdd() ? 1 : 0);
        billingDirectionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dbTracker.setIfAdd(position == 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupDBNotificationTracker() {
        dbTracker = db.findNotificationTrackerByProjectId(dbProject.getId());
        List<DBMember> memberList = db.getMembersOfProject(dbProject.getId(), null);
        if (dbTracker == null) {
            dbTracker = new DBNotificationTracker(
                    -1,
                    dbProject.getId(),
                    memberList
                            .get(0).getId(),
                    memberList
                            .get(1).getId(),
                    false,
                    true,
                    true,
                    "",
                    "\\b\\d+\\.\\d+\\b",
                    null
            );
        }
        dbTracker.setTrackedUserId(getCurrentUserMemberId());
    }


    private void setupBillableUser(Spinner editBillableUser) {
        List<DBMember> memberList = db.getMembersOfProject(dbProject.getId(), null);
        List<String> payerNameList = new ArrayList<>();
        List<String> payerIdList = new ArrayList<>();
        int selectedMember = -1;
        int index = 0;
        for (DBMember member : memberList) {
            if (member.isActivated() || member.getId() == dbProject.getId()) {
                payerNameList.add(member.getName());
                payerIdList.add(String.valueOf(member.getId()));
            }
            if (member.getId() == dbTracker.getBillableUserId()) {
                selectedMember = index;
            }
            index++;
        }

        List<UserItem> userList = new ArrayList<>();
        for (int i = 0; i < payerNameList.size(); i++) {
            userList.add(new UserItem(Long.valueOf(payerIdList.get(i)), payerNameList.get(i)));
        }
        SpinnerAdapter userAdapter = new UserAdapter(this, userList);
        editBillableUser.setAdapter(userAdapter);
        // TODO: set selected value for payer
        if (selectedMember != -1) {
            editBillableUser.setSelection(selectedMember);
        }
        editBillableUser.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "PAYERRRR");
//                get {position} member
                dbTracker.setBillableUserId(memberList.get(position).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(TAG, "PAYERRRR NOTHING");

            }
        });
    }

    private void loadPermissionsStatus() {
        hasNotificationPermissions = isNotificationServiceRunning();
        Log.v(NotificationTrackerActivity.class.getSimpleName(), "hasNotificationPermission: " + hasNotificationPermissions);
    }

    public void goToNotificationPermissionSettings() {
        startActivity(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS));
    }

    private void updateView() {
        if (hasNotificationPermissions) {
            trackingPermissionEnabledContainer.setVisibility(View.VISIBLE);
            trackingPermissionDisabledContainer.setVisibility(View.GONE);
        } else {
            trackingPermissionEnabledContainer.setVisibility(View.GONE);
            trackingPermissionDisabledContainer.setVisibility(View.VISIBLE);
        }
    }

    private void saveOrUpdateTracker() {
//        save tracker to DB
        long changedRecordsCount = 0;
//        TODO: get databasehelper
        if (dbTracker.getId() == -1) {
            changedRecordsCount = db.addNotificationTracker(dbTracker);
        } else {
            changedRecordsCount = db.updateNotificationTracker(dbTracker);
        }

        stopService(new Intent(this, NotificationTrackerService.class));
        startService(new Intent(this, NotificationTrackerService.class));
        if (changedRecordsCount > 0) {
            Toast toast = Toast.makeText(this, "Notification tracker updated", Toast.LENGTH_SHORT);
            toast.show();
            finish();
        } else {
            Toast toast = Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT);
            toast.show();
        }
    }


    public boolean isNotificationServiceRunning() {
        ContentResolver contentResolver = getContentResolver();
        String enabledNotificationListeners =
                Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        String packageName = getPackageName();
        return enabledNotificationListeners != null && enabledNotificationListeners.contains(packageName);
    }
}