package net.eneiluj.moneybuster.android.ui;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import net.eneiluj.moneybuster.R;
import net.eneiluj.moneybuster.model.DBProject;

import java.util.List;

public class PackagesAdapter extends ArrayAdapter<PackageInfo> {
    private final Activity mContext;
    private final List<PackageInfo> mValues;
    private final int mSelectedItem;

    public PackagesAdapter(Activity context, List<PackageInfo> values, int checkedItem) {
        super(context, R.layout.package_item, R.id.name, values);
        this.mContext = context;
        this.mValues = values;
        this.mSelectedItem = checkedItem;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        PackageViewHolderItem viewHolder;
        View view = convertView;
        viewHolder = new PackageViewHolderItem();

        if (view == null) {
            LayoutInflater inflater = mContext.getLayoutInflater();
            view = inflater.inflate(R.layout.package_item, parent, false);

            viewHolder.layout = view.findViewById(R.id.packageItemLayout);
            viewHolder.icon = view.findViewById(R.id.icon);
            viewHolder.name = view.findViewById(R.id.name);

            view.setTag(viewHolder);
        }
        PackageInfo packageInfo = mValues.get(position);
        if (packageInfo != null && viewHolder.name != null) {
            viewHolder.name.setText(packageInfo.packageName);
            viewHolder.icon.setTag(packageInfo.applicationInfo.icon);
        }
        return view;
    }

    /**
     * Package ViewHolderItem to get smooth rendering.
     */
    private static class PackageViewHolderItem {
        private ImageView icon;
        private TextView name;
        private LinearLayout layout;
    }
}
