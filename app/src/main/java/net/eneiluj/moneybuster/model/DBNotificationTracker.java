package net.eneiluj.moneybuster.model;

import java.io.Serializable;

public class DBNotificationTracker implements Serializable {
    private long id;
    private long projectId;
    private long billableUserId;
    private long trackedUserId;
    private boolean ifAdd;
    private boolean ifLogToFile;
    private boolean enabled;
    private String titleMatcher;
    private String valueMatcher;
    private String packageId;

    public DBNotificationTracker(long id,
                                 long projectId,
                                 long trackedUserId,
                                 long billableUserId,
                                 boolean ifAdd,
                                 boolean ifLogToFile,
                                 boolean enabled,
                                 String titleMatcher,
                                 String valueMatcher,
                                 String packageId) {
        this.id = id;
        this.projectId = projectId;
        this.billableUserId = billableUserId;
        this.trackedUserId = trackedUserId;
        this.ifAdd = ifAdd;
        this.ifLogToFile = ifLogToFile;
        this.enabled = enabled;
        this.titleMatcher = titleMatcher;
        this.valueMatcher = valueMatcher;
        this.packageId = packageId;
    }

    public String getTitleMatcher() {
        return titleMatcher;
    }

    public void setTitleMatcher(String titleMatcher) {
        this.titleMatcher = titleMatcher;
    }

    public String getValueMatcher() {
        return valueMatcher;
    }

    public void setValueMatcher(String valueMatcher) {
        this.valueMatcher = valueMatcher;
    }

    public boolean isIfLogToFile() {
        return ifLogToFile;
    }

    public void setIfLogToFile(boolean ifLogToFile) {
        this.ifLogToFile = ifLogToFile;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public long getProjectId() {
        return projectId;
    }

    public void setProjectId(long projectId) {
        this.projectId = projectId;
    }

    public long getBillableUserId() {
        return billableUserId;
    }

    public void setBillableUserId(long billableUserId) {
        this.billableUserId = billableUserId;
    }

    public long getTrackedUserId() {
        return trackedUserId;
    }

    public void setTrackedUserId(long trackedUserId) {
        this.trackedUserId = trackedUserId;
    }

    public boolean isIfAdd() {
        return ifAdd;
    }

    public void setIfAdd(boolean ifAdd) {
        this.ifAdd = ifAdd;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean getAddToUser() {
        return ifAdd;
    }

    @Override
    public String toString() {
        return "DBNotificationTracker{" +
                "id=" + id +
                ", projectId=" + projectId +
                ", billableUserId=" + billableUserId +
                ", trackedUserId=" + trackedUserId +
                ", addToUser=" + ifAdd +
                '}';
    }
}
